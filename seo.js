const cheerio = require('cheerio');
const fs = require('fs');

var data = '';

module.exports = {
	readStream : SeoStream,
	readFile : SeoFile
}

function Seo(data) {
	this.data = data
	this.createCheerio(this);
}

function SeoFile(path) {
	this.data = fs.readFileSync(path);
	this.createCheerio(this);
}

function SeoStream(stream,completionCallback) {	
	var self = this;
	getDataFromStream(stream,function(data) {
		self.data = data;
		self.createCheerio(self);
		completionCallback(self);
	});
}

function getDataFromStream(stream,callback) {
	var str = '';
	stream.on('data', function(chunk){
    		str += chunk;
	});
	stream.on('end', function(){
		callback(str);
	});
}

Seo.prototype = {
	constructor: Seo,
	createCheerio: function(context) {
		context.$ = cheerio.load(context.data);
		context.result = '';
	},
	detectImgWithoutAlt: function() {
		var altMissing = 0;
		this.$('img').each(function (index, element) {
		var alt = element.attribs.alt;
		if(alt === undefined)
			altMissing++;
		});
		this.result += 'There are ' + altMissing + ' <img> tag without alt attribute\n';
		return this;
	},
	detectAWithoutRel: function() {
		var relMissing = 0;
		this.$('a').each(function (index, element) {
			var rel = element.attribs.rel;
			if(rel === undefined)
				relMissing++;
		});
		this.result += 'There are ' + relMissing + ' <a> tag without rel attribute\n';
		return this;
	},
	detectHead: function() {
		if(this.$('title').text() == '')
			this.result += 'This HTML without <title> tag\n';
		var description = true;
		var keywords = true;
		this.$('meta[name]').each(function (index, element) {
			var name = element.attribs.name;
			if(name == 'description')
				description = false;
			if(name == 'keywords')
				keywords = false;
		});
		if(description)
			this.result += 'This HTML without <meta name="descriptions" ... /> tag\n';
		if(keywords)
			this.result += 'This HTML without <meta name="keywords" ... /> tag\n';
		return this;
	},
	detectStrongMoreThanNum: function(num) {
		var defaultNum = 15;
		if(num !== undefined)
			defaultNum = num;
		if(this.$('strong').get().length > defaultNum) 
			this.result += 'This HTML have more than ' + defaultNum + ' <strong> tag\n';
		return this;
	},
	detectH1MoreThanOne: function() {
		var h1 = this.$('h1').get().length;
		if(h1 > 1) 
			this.result += 'This HTML have more than one <h1> tag\n';
		return this;
	},
	detectCustomMeta: function(customTag) {
		var customRule = true;
		this.$('meta[name]').each(function (index, element) {
			var name = element.attribs.name;
			if(name == customTag)
				customRule = false;
		});
		if(customRule)
			this.result += 'This HTML without <meta name="' + customTag + '" ... /> tag\n';
		else
			this.result += 'This HTML existing <meta name="' + customTag + '" ... /> tag\n';
		return this;
	},
	toConsole: function() {
		console.log(this.result);
		return this;
	},
	writeFile: function(path) {
		var defaultPath = 'seo.log';
		if(path !== undefined)
			defaultPath = path;
		fs.writeFile(defaultPath, this.result, 'utf8', function(err) {
			if(err)
				return console.log(err);
			console.log("The result was saved at: " + path);
		});
		return this;
	},
	writeStream: function(stream) {
		stream.write(this.result, 'UTF8');
		stream.end();
		stream.on('finish', function() {
    			console.log("Write stream completed.");
		});
		return this;
	}
}


SeoStream.prototype = Object.create(Seo.prototype);
SeoStream.prototype.constructor = SeoStream;
SeoFile.prototype = Object.create(Seo.prototype);
SeoFile.prototype.constructor = SeoFile;
