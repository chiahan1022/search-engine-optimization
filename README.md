# Search Engine Optimization
Node.js Developer can use this package to scan a HTML file and show all of the SEO defects.

## Installation

```bash
npm install search-engine-optimization
```

## SEO Rules
### .detectImgWithoutAlt()
Detect if any \<img /> tag without alt attribute.

### .detectAWithoutRel()
Detect if any \<a /> tag without rel attribute.

### .detectHead()
In \<head> tag  
Detect if header doesn’t have \<title> tag.  
Detect if header doesn’t have \<meta name=“descriptions” ... /> tag.  
Detect if header doesn’t have \<meta name=“keywords” ... /> tag.

### .detectStrongMoreThanNum(Number)
Detect if there’re more than config number \<strong> tag in HTML (default value is 15).
```javascript
.detectStrongMoreThanNum(15)
```

### .detectH1MoreThanOne()
Detect if a HTML have more than one \<H1> tag.

### .detectCustomMeta(Custom Rule)
Detect custom additional rules for \<meta> tag
```javascript
.detectCustomMeta('robots')
```

### .toConsole()
The result output to console.

### .writeFile(File Path)
The result output to a file.
```javascript
.writeFile('FileOutput.log')
```

### .writeStream(Writer Stream)
The result output to writable stream.
```javascript
.writeStream(writerStream)
```

## Example        
### Free to chain any rules by yourself
* User can only use the rule 1 and 4 or only use rule 2.
```javascript
var seo = require('search-engine-optimization')
var seoFromFile = new seo.readFile('sample.html')
        .detectImgWithoutAlt()
        .detectStrongMoreThanNum(15)
        .toConsole()
```
```javascript
var seo = require('search-engine-optimization')
var seoFromFile = new seo.readFile('sample.html')
        .detectAWithoutRel()
        .toConsole()
```
* The order of rules doesn’t matter
```javascript
var seo = require('search-engine-optimization')
var seoFromFile = new seo.readFile('sample.html')
        .detectStrongMoreThanNum(15)
        .detectImgWithoutAlt()
        .toConsole()
```
```javascript
var seo = require('search-engine-optimization')
var seoFromFile = new seo.readFile('sample.html')
        .detectImgWithoutAlt()
        .detectStrongMoreThanNum(15)
        .toConsole()
```
### The input can be:
* A HTML file (User is able to config the input path)
```javascript
var seo = require('search-engine-optimization')
var seoFromFile = new seo.readFile('sample.html')
        .detectH1MoreThanOne()
        .toConsole()
```
* Node Readable Stream
```javascript
var seo = require('search-engine-optimization')
var fs = require("fs")
var readerStream = fs.createReadStream('sample.html');
readerStream.setEncoding('UTF8');
var seoFromStream = new seo.readStream(readerStream,function(mod) {
     mod.detectH1MoreThanOne()
        .toConsole()
});
```
### The output can be:
* A file (User is able to config the output destination)
```javascript
var seo = require('search-engine-optimization')
var seoFromFile = new seo.readFile('sample.html')
        .detectH1MoreThanOne()
        .writeFile('FileOutput.log')
```
* Node Writable Stream
```javascript
var seo = require('search-engine-optimization')
var fs = require("fs")
var writerStream = fs.createWriteStream('WriteStreamOutput.log');
var seoFromFile = new seo.readFile('sample.html')
        .detectH1MoreThanOne()
        .detectCustomMeta('robots')
        .writeStream(writerStream)
```
* Console
```javascript
var seo = require('search-engine-optimization')
var seoFromFile = new seo.readFile('sample.html')
        .detectH1MoreThanOne()
        .toConsole()
```
### Flexible to add rules for <meta> tag.
* Ex: Checking <meta name=“robots” /> existing or not?!
```javascript
var seo = require('search-engine-optimization')
var seoFromFile = new seo.readFile('sample.html')
        .detectCustomMeta('robots')
        .toConsole()
```

## Example Output
Following is a simple output demo when a user apply above 5 rules.  
![example](https://docs.google.com/uc?id=1tOmKM_wDNTcv3JoVkY0EYt08T1iSCJPd "Example Output")

## License
This project is licensed under the MIT License.

## Acknowledgements
* [cheeriojs/cheerio](https://github.com/cheeriojs/cheerio)